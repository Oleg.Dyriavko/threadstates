package org.dataArt.multithreading;
/**
 * Написать программу, которая гарантированно выводит на консоль все состояния потока.
 * */
public class ThreadStates {
    public static void main(String[] args) throws InterruptedException {
        Thread sleepingThread = new SleepingThread();
        System.out.println("(1) Sleeping thread state: " + sleepingThread.getState());
        sleepingThread.start();
        System.out.println("(2) Sleeping thread state: " + sleepingThread.getState());
        Thread.sleep(2000);
        System.out.println("(3) Sleeping thread state: " + sleepingThread.getState());
        sleepingThread.join();
        System.out.println("(4) Sleeping thread state: " + sleepingThread.getState());
        System.out.println("=====================================================");

        Thread waitingThread = new WaitingThread();
        System.out.println("(1) Waiting thread state: " + waitingThread.getState());
        waitingThread.start();
        Thread.sleep(50);
        synchronized (waitingThread) {
            System.out.println("(2) Waiting thread state: " + waitingThread.getState());
            waitingThread.notify();
            System.out.println("(3) Waiting thread state: " + waitingThread.getState());
        }
        System.out.println("=====================================================");

        Thread joiningThread = new JoiningThread();
        System.out.println("(1) Main thread state: " + Thread.currentThread().getState());
        Thread thirdThread = new Thread(() -> {
            try {
                System.out.println("Started third thread");
                joiningThread.join();
                System.out.println("Finished third thread");
            } catch (InterruptedException e) {
                //do nothing
            }
        });
        joiningThread.start();
        thirdThread.start();
        Thread.sleep(100);
        System.out.println("(2) Joining thread state: " + joiningThread.getState());
        System.out.println("(3) Third thread state: " + thirdThread.getState());
    }

    static class SleepingThread extends Thread {

        @Override
        public void run() {
            System.out.println("We are inside the Sleeping Thread");
            try {
                sleep(4000);
                System.out.println("Finished sleeping");
            } catch (InterruptedException e) {
                System.out.println("Oh, someone interrupted me!");
            }
        }
    }

    static class WaitingThread extends Thread {

        @Override
        public void run() {
            System.out.println("We are inside the Waiting Thread");

            synchronized (this) {
                try {
                    wait();
                    System.out.println("Finished waiting");
                } catch (InterruptedException e) {
                    System.out.println("Oh, someone interrupted me!");
                }
            }
        }
    }

    static class JoiningThread extends Thread {

        @Override
        public void run() {
            System.out.println("We are inside the Joining Thread");
            try {
                sleep(3000);
                System.out.println("Finished joining thread");
            } catch (InterruptedException e) {
                System.out.println("Oh, someone interrupted me!");
            }
        }
    }
}